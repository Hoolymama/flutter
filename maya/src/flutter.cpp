/*
*  flutter.cpp
	*  jtools
	*
	*  Created by Julian Mann on 02/12/2006.
	*  Copyright 2006 hooly|mama. All rights reserved.
	*
*/

//#include <boost/shared_ptr.hpp>

#include <math.h>
#include <vector>

#include <maya/MTime.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MPointArray.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MAnimControl.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MEulerRotation.h>
#include <maya/MVector.h>
#include <maya/MPoint.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MMatrix.h>
#include <maya/MGlobal.h>
#include <maya/MPlugArray.h>
#include <maya/MQuaternion.h>
#include <maya/MDataBlock.h>
//#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>

#include "flutter.h"
#include "errorMacros.h"

#include "dynMesh.h"
#include "jMayaIds.h"

//#include "attributeInlines.h"

#define ROTATE_ORDER_XYZ	0
#define ROTATE_ORDER_YZX	1
#define ROTATE_ORDER_ZXY	2
#define ROTATE_ORDER_XZY	3
#define ROTATE_ORDER_YXZ	4
#define ROTATE_ORDER_ZYX	5
#define OUTPUT_RADIANS	0
#define OUTPUT_DEGREES	1

#define ROT_TO_DEG  57.295779524

//const double PI_X2 = 6.283185306;

//using namespace std;

typedef std::vector<dynMesh*> DYN_MESH_LIST;

inline MMatrix calcMat(const MVector &p, const MVector &phi ) {
	// given a vector representing translation
	// and a vector representing rotation (axis, length = angle in radians)
	// use a quaternion to calculate a matrix
	double len = phi.length();
	MQuaternion q;
	q.setAxisAngle(phi, len);
	MMatrix m = q.asMatrix();
	m(3,0) = p.x;
	m(3,1) = p.y;
	m(3,2) = p.z;
	return m;
}

inline MVector calcRotation(
	const MVector &phi,
	MEulerRotation::RotationOrder ord, 
	short outUnit
){
	double len = phi.length();
	MQuaternion q;
	q.setAxisAngle(phi, len);
	MEulerRotation euler(MVector::zero, ord);
	euler = q;
	// cerr << "euler.asVector(): " << euler.asVector() << endl;
	if (outUnit == OUTPUT_DEGREES){
		return (euler.asVector() * ROT_TO_DEG);
	}
	return euler.asVector() ;

}

inline MVector  crossProdSum(
	const MVector &p1,
	const MVector &p2,
	const MVector &p3
	)
{
	return (p1^p2)+(p2^p3)+(p3^p1);
}

inline MVector triCenter( 	
	const MVector &p1,
	const MVector &p2,
	const MVector &p3
	)
{
	return ((p1 + p2 +p3 ) * 0.33333333333333);
}


MObject flutter::aPosition;
MObject flutter::aVelocity;
MObject flutter::aMass;
MObject flutter::aAcceleration;
MObject flutter::aEvent;
MObject flutter::aPhi;
MObject flutter::aOmega;
MObject flutter::aMeshIndex;
MObject flutter::aForceMagPP; 
MObject flutter::aTorqueMagPP; 
MObject flutter::aTorqueConservePP;

MObject flutter::aCurrentTime;

MObject flutter::aInputMeshes;
MObject flutter::aNormalize;
MObject flutter::aForceMag; 
MObject flutter::aTorqueMag; 
MObject flutter::aTorqueConserve; 
MObject flutter::aDoBounce;
MObject flutter::aBounceSpin; 
MObject flutter::aBounceSpinThreshold; 
MObject flutter::aBounceDeflect;

MObject flutter::aIRotateOrder;
MObject flutter::aOutputUnit;

MObject flutter::aOutForce; 
MObject flutter::aOutPhi;    
MObject flutter::aOutOmega;  
MObject flutter::aOutRotation;

// MObject flutter::aExtraOutData;   

MTypeId flutter::id( k_flutter );

flutter::flutter(){
	//lastTimeIEvaluated = MAnimControl::currentTime();
	lastTimeIEvaluated = MAnimControl::currentTime();

}

flutter::~flutter(){}

void *flutter::creator()
{	
	return new flutter;
}


MStatus flutter::initialize()
//
//	Descriptions:
//		Initialize the node, attributes.
//
{
	MStatus st;
	MString method("flutter::initialize");

	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnCompoundAttribute	cAttr;
	MFnUnitAttribute	uAttr;
	MFnEnumAttribute eAttr;


	aPosition = tAttr.create("position", "pos", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aPosition);er;

	aVelocity = tAttr.create("velocity", "vel", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aVelocity);er;

	aMass = tAttr.create("mass", "mas", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aMass);er;

	aAcceleration = tAttr.create("acceleration", "acc", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aAcceleration);er;

	aEvent = tAttr.create("event", "evt", MFnData::kDoubleArray);
	tAttr.setStorable(false);	
	tAttr.setReadable(false);
	st = addAttribute(aEvent);er;

	aPhi = tAttr.create("phi", "ph", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aPhi);er;

	aOmega= tAttr.create("omega", "om", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aOmega);er;

	aMeshIndex = tAttr.create("meshIndex", "mi", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aMeshIndex);er;

	aForceMagPP = tAttr.create("forceMagPP", "fmpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aForceMagPP);er;

	aTorqueMagPP = tAttr.create("torqueMagPP", "tmpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aTorqueMagPP);er;

	aTorqueConservePP = tAttr.create("torqueConservePP", "tcpp", MFnData::kDoubleArray);
	tAttr.setStorable(false);
	tAttr.setReadable(false);
	st = addAttribute(aTorqueConservePP);er;

	aInputMeshes = tAttr.create("inputMeshes", "imsh", MFnData::kMesh);
	tAttr.setStorable(false);
	tAttr.setArray(true);
	st = addAttribute(aInputMeshes);er;	

	aNormalize = nAttr.create("normalizeSurfaceArea", "nmz", MFnNumericData::kBoolean);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(true);
	st = addAttribute(aNormalize);er;

	aForceMag = nAttr.create("dragForce", "drg", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aForceMag);er;

	aTorqueMag = nAttr.create("dragSpin", "drsp", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aTorqueMag);er;

	aTorqueConserve = nAttr.create("conserveSpin", "csp", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aTorqueConserve);er;

	aDoBounce = nAttr.create("spinOnCollisions", "soc", MFnNumericData::kBoolean);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(true);
	st = addAttribute(aDoBounce);er;

	aBounceSpin = nAttr.create("collisionSpinFactor", "csf", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aBounceSpin);er;

	aBounceSpinThreshold = nAttr.create("collisionSpinThreshold", "csth", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(1.0);
	st = addAttribute(aBounceSpinThreshold);er;

	aBounceDeflect = nAttr.create("collisionDeflection", "cdf", MFnNumericData::kDouble);	
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setDefault(0.0);
	st = addAttribute(aBounceDeflect);er;

	aCurrentTime = uAttr.create( "currentTime", "ct", MFnUnitAttribute::kTime );
	uAttr.setInternal( true );
	st =	addAttribute(aCurrentTime);	er;

	aIRotateOrder = eAttr.create( "instanceRotateOrder", "iro", ROTATE_ORDER_XYZ);
	eAttr.addField("xyz", ROTATE_ORDER_XYZ);
	eAttr.addField("yzx", ROTATE_ORDER_YZX);
	eAttr.addField("zxy", ROTATE_ORDER_ZXY);
	eAttr.addField("xzy", ROTATE_ORDER_XZY);
	eAttr.addField("yxz", ROTATE_ORDER_YXZ);
	eAttr.addField("zyx", ROTATE_ORDER_ZYX);
	eAttr.setKeyable(true);
	eAttr.setHidden(false);
	st = addAttribute( aIRotateOrder );er;

	aOutputUnit = eAttr.create( "angularUnit", "ang", OUTPUT_DEGREES);
	eAttr.addField("radians", OUTPUT_RADIANS);
	eAttr.addField("degrees",OUTPUT_DEGREES);
	uAttr.setKeyable(true);
	uAttr.setHidden(false);
	st = addAttribute( aOutputUnit );er;

	aOutForce = tAttr.create("outForce", "ofc", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aOutForce);er;

	aOutPhi = tAttr.create("outPhi", "oph", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aOutPhi);er;

	aOutOmega= tAttr.create("outOmega", "oom", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aOutOmega);er;

	aOutRotation = tAttr.create("outRotation", "orot", MFnData::kVectorArray , &st ); er;
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	st = addAttribute(aOutRotation);er;


	st = attributeAffects(aCurrentTime, aOutForce );er;
	st = attributeAffects(aCurrentTime, aOutPhi );er;
	st = attributeAffects(aCurrentTime, aOutOmega);er;
	st = attributeAffects(aCurrentTime, aOutRotation);er;


	return( MS::kSuccess );
}


MStatus flutter::compute(const MPlug& plug, MDataBlock& data)
//
//	Descriptions:
//		compute output force.
//

{

	MStatus st;

	//cerr << __FILE__ << " " << __LINE__ << endl;
	if( !(
		(plug == aOutForce) ||
		(plug == aOutPhi) ||
		(plug == aOutOmega) ||
		(plug == aOutRotation)
		))return( MS::kUnknownParameter);

	double forceMagValue = data.inputValue(aForceMag).asDouble();
	double torqueMagValue = data.inputValue(aTorqueMag).asDouble();
	if ((forceMagValue == 0.0) && (torqueMagValue == 0.0)) {
		return( MS::kUnknownParameter);
	}


	// get time parameters
	////////////////////////////////////////////////////////////////
	MTime cT ,dT;
	cT = data.inputValue(aCurrentTime).asTime();
	dT = cT - lastTimeIEvaluated;
	double dt = dT.as( MTime::kSeconds );
	lastTimeIEvaluated = cT;
	////////////////////////////////////////////////////////////////



	

	MDataHandle hPosition = data.inputValue( aPosition, &st );er;
	MObject dPosition = hPosition.data();
	MFnVectorArrayData fnPosition( dPosition );
	MVectorArray points = fnPosition.array( &st );er;


	MDataHandle hVelocity = data.inputValue( aVelocity, &st );er;
	MObject dVelocity = hVelocity.data();
	MFnVectorArrayData fnVelocity( dVelocity );
	MVectorArray velocities = fnVelocity.array( &st );er;

	MDataHandle hMass = data.inputValue( aMass, &st );er;
	MObject dMass = hMass.data();
	MFnDoubleArrayData fnMass( dMass );
	MDoubleArray mass = fnMass.array( &st);er;

	MDataHandle hAcceleration = data.inputValue(aAcceleration, &st);er;
	MObject dAcceleration = hAcceleration.data();
	MFnVectorArrayData fnAcceleration( dAcceleration );
	MVectorArray accelerations = fnAcceleration.array();

	MDataHandle hEvent = data.inputValue(aEvent, &st);er;
	MObject dEvent = hEvent.data();
	MFnDoubleArrayData fnEvent( dEvent );
	MDoubleArray events = fnEvent.array();

	MDataHandle hPhi = data.inputValue(aPhi, &st);er;
	MObject dPhi = hPhi.data();
	MFnVectorArrayData fnPhi( dPhi );
	MVectorArray phi = fnPhi.array( &st );er;

	MDataHandle hOmega = data.inputValue(aOmega, &st);er;
	MObject dOmega = hOmega.data();
	MFnVectorArrayData fnOmega( dOmega );
	MVectorArray omega = fnOmega.array( &st );er;

	MDataHandle hMeshIndex = data.inputValue(aMeshIndex, &st);er;
	MObject dMeshIndex = hMeshIndex.data();
	MFnDoubleArrayData fnMeshIndex( dMeshIndex );
	MDoubleArray meshIndex = fnMeshIndex.array( &st);er;

	MDataHandle hForceMagPP = data.inputValue(aForceMagPP, &st);er;
	MObject dForceMagPP = hForceMagPP.data();
	MFnDoubleArrayData fnForceMagPP( dForceMagPP , &st); er;
	MDoubleArray forceMagPP = fnForceMagPP.array( &st);er;

	MDataHandle hTorqueMagPP = data.inputValue(aTorqueMagPP, &st);er;
	MObject dTorqueMagPP = hTorqueMagPP.data();
	MFnDoubleArrayData fnTorqueMagPP( dTorqueMagPP );
	MDoubleArray torqueMagPP = fnTorqueMagPP.array( &st);er;

	MDataHandle hTorqueConservePP = data.inputValue(aTorqueConservePP, &st);er;
	MObject dTorqueConservePP = hTorqueConservePP.data();
	MFnDoubleArrayData fnTorqueConservePP( dTorqueConservePP );
	MDoubleArray torqueConservePP = fnTorqueConservePP.array( &st);er;
	// cerr << __FILE__ << " " << __LINE__ << endl;

	MVectorArray  outForce;
	MVectorArray  outPhi;
	MVectorArray  outOmega;
	MVectorArray  outRotation;
	// cerr << __FILE__ << " " << __LINE__ << endl;

	st = calculateForce( 
		dt,
		data, 
		points,
		velocities,
		accelerations,
		events,
		phi,
		omega,
		meshIndex,
		forceMagPP,
		torqueMagPP,
		torqueConservePP,
		outForce, 
		outPhi,
		outOmega,
		outRotation
		);


	MDataHandle hOut = data.outputValue( aOutForce, &st);er;
	MFnVectorArrayData fnOutput;
	MObject dOutput = fnOutput.create(outForce , &st );er;
	hOut.set( dOutput);
	data.setClean( aOutForce );

	MDataHandle hOutPhi = data.outputValue(aOutPhi, &st);er;
	MFnVectorArrayData fnOutPhi;
	MObject dOutPhi = fnOutPhi.create( outPhi, &st );er;
	hOutPhi.set( dOutPhi );
	data.setClean( aOutPhi);

	MDataHandle hOutOmega = data.outputValue(aOutOmega, &st);er;
	MFnVectorArrayData fnOutOmega;
	MObject dOutOmega = fnOutOmega.create( outOmega, &st );er;
	hOutOmega.set( dOutOmega );
	data.setClean( aOutOmega);

	MDataHandle hOutRotation = data.outputValue(aOutRotation, &st);er;
	MFnVectorArrayData fnOutRotation;
	MObject dOutRotation = fnOutRotation.create( outRotation, &st );er;
	hOutRotation.set( dOutRotation );
	data.setClean( aOutRotation);






	return( MS::kSuccess );
}
MStatus flutter::calculateForce
	(
	double dt,
	MDataBlock &data, 
	const MVectorArray &points,
	const MVectorArray &velocities,
	const MVectorArray &accelerations,
	const MDoubleArray &events,
	const MVectorArray &phi,
	const MVectorArray &omega,
	const MDoubleArray &meshIndex,
	const MDoubleArray &forceMagPP,
	const MDoubleArray &torqueMagPP,
	const MDoubleArray &torqueConPP,
	MVectorArray &outForce ,
	MVectorArray &outPhi,
	MVectorArray &outOmega,
	MVectorArray &outRotation
	)
{
	MStatus st;
	MString method("flutter::calculateForce");

	// scalars etc.	
	////////////////////////////////////////////////////
	bool nomalizeValue = data.inputValue(aNormalize).asBool();
	double forceMagValue = data.inputValue(aForceMag).asDouble();
	double torqueMagValue = data.inputValue(aTorqueMag).asDouble();
	double torqueConValue = data.inputValue(aTorqueConserve).asDouble();
	bool doBounceValue = data.inputValue(aDoBounce).asBool();
	double bounceSpinValue = data.inputValue(aBounceSpin).asDouble();
	double bounceSpinThreshValue = data.inputValue(aBounceSpinThreshold).asDouble();
	double bounceDeflectValue = data.inputValue(aBounceDeflect).asDouble();
	short rotateOrder = data.inputValue(aIRotateOrder).asShort();
	short outUnit =  data.inputValue(aOutputUnit).asShort();
	////////////////////////////////////////////////////

	// rotate order
	////////////////////////////////////////////////////
	MEulerRotation::RotationOrder ord;
	switch ( rotateOrder ) {
		case ROTATE_ORDER_XYZ:
		ord = MEulerRotation::kXYZ; break;
		case ROTATE_ORDER_YZX:
		ord = MEulerRotation::kYZX; break;
		case ROTATE_ORDER_ZXY:
		ord = MEulerRotation::kZXY; break;
		case ROTATE_ORDER_XZY:
		ord = MEulerRotation::kXZY; break;
		case ROTATE_ORDER_YXZ:
		ord = MEulerRotation::kYXZ; break;
		case ROTATE_ORDER_ZYX:
		ord = MEulerRotation::kZYX; break;
		default:
		ord = MEulerRotation::kXYZ; break;
	}
	////////////////////////////////////////////////////

	if (!
		( 
		forceMagValue || 
		torqueMagValue || 
		(doBounceValue && (bounceSpinValue || bounceDeflectValue ))
		)
		) return MS::kFailure;

	unsigned len = points.length();

	if (!(
		len  &&
		(velocities.length() == len)  &&
		(meshIndex.length() == len)  &&
		(phi.length() == len)  &&
		(omega.length() == len) 
		)) return MS::kFailure;

	MDoubleArray forceMagArray(len,forceMagValue);
	MDoubleArray torqueMagArray(len,torqueMagValue);
	MDoubleArray torqueConArray(len,torqueConValue);

  	// mult the force and torque arrays by their PP multipliers if they exist
	if ((forceMagValue != 0.0) && (forceMagPP.length() == len)) {
		for (unsigned i = 0;i<len;i++) {forceMagArray[i] = forceMagArray[i] * forceMagPP[i];}
	}
	if ((torqueMagValue != 0.0) && (torqueMagPP.length() == len)) {
		for (unsigned i = 0;i<len;i++) {torqueMagArray[i] = torqueMagArray[i] *  torqueMagPP[i];}
	}
	if ((torqueConValue != 0.0) && (torqueConPP.length() == len)) {
		for (unsigned i = 0;i<len;i++) {torqueConArray[i] = torqueConArray[i] *  torqueConPP[i];}
	}

	// build the DYN_MESH_LIST
	////////////////////////////////////////////////////
	DYN_MESH_LIST meshList;
	MArrayDataHandle hInputMeshes = data.inputArrayValue( aInputMeshes, &st ); er;
	unsigned int count = hInputMeshes.elementCount(&st); er;
	if (!count) {
		MGlobal::displayError("not enough meshes"); 
		return MS::kFailure;
	}
	do { 
		//unsigned int el = hInputMeshes.elementIndex(&st);  er;
		if (st == MS::kSuccess) {
			MDataHandle hMesh = hInputMeshes.inputValue( &st );er;	
			MObject  dMesh = hMesh.data();
			dynMesh * m = new dynMesh(dMesh, nomalizeValue);
			meshList.push_back(m);
		}
	} while (hInputMeshes.next() == MS::kSuccess );
	////////////////////////////////////////////////////

	if (!((accelerations.length() == len) && (events.length() == len)))  doBounceValue = false;


	MMatrix mat;

	for (unsigned i=0;i<len;i++) {
		//cerr <<< "in loop " << endl;
		// get the transform matrix and multiply the points

		mat = calcMat(points[i], phi[i]);

		MVector localVelocity = (velocities[i] * mat.inverse());
		MVector localOmega = (omega[i] * mat.inverse());

		int mi = (int(meshIndex[i]) % meshList.size());
		// dynMesh &dynMesh = *(meshList[mi]);
		MVector torque(0.0,0.0,0.0);
		MVector force(0.0,0.0,0.0);

		(meshList[mi])->calcForceAndTorque(
			dt,
			localVelocity, 
			localOmega, 
			//forceMagValue,
			//torqueMagValue,
			forceMagArray[i],
			torqueMagArray[i],
			force, 
			torque 
			);


		if (doBounceValue) {
			if (events[i]){
				if (bounceSpinThreshValue < velocities[i].length() ) {
					MVector bounceForce, bounceTorque;
					MVector localAccel = (accelerations[i]* mat.inverse());
					(meshList[mi])->calcBounceVectors(
						localVelocity,
						localAccel,
						bounceSpinValue ,
						bounceDeflectValue ,
						bounceForce,
						bounceTorque
						);
					// cerr <<" bounceForce " << bounceForce << endl;
					force += bounceForce;
					torque += bounceTorque;
				}
			}
		}

		force =  (force*mat);
		torque = (torque*mat);


		MVector deltaOmega = torque*dt;
		MVector outO = (omega[i]*torqueConArray[i]) + deltaOmega;
				// 	MVector outO = (omega[i]*torqueConValue) + deltaOmega;

		MVector deltaPhi = outO * dt; 	
		MVector outP = phi[i] +  deltaPhi;


		outOmega.append(outO);
		outPhi.append(outP);
		outForce.append(force);

		// we can work out rotation from outP 		
		MVector vOut = calcRotation(outP,ord,outUnit);
		outRotation.append(vOut);
	}

	// delete the meshes
	DYN_MESH_LIST::iterator iter = meshList.begin();
	while (iter != meshList.end()) {
		delete *iter; *iter=0;
		iter++;
	}


	return MS::kSuccess;
}

