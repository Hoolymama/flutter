/*
 *  dynMesh.h
 *  jtools
 *
 *  Created by Julian Mann on 02/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */
#ifndef _JM_DYN_MESH
#define _JM_DYN_MESH

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
 


class dynMesh
{
public:
	dynMesh() ;
	dynMesh(const MObject &meshObject, bool normalize) ;
	
	~dynMesh();
	// void calcForceAndTorque(const MVector &localVelocity, MVector &force, MVector &torque) ;
	void calcForceAndTorque(double dt,
							const MVector &localVelocity,
							const MVector &localOmega, 
							const double &forceMag,
							const double & torqueMag,
							MVector &force, 
							MVector &torque
							) ;
	void calcForceAndTorque(
							const MVector &localVelocity,
							const MVector &localOmega, 
							const double &forceMag,
							const double & torqueMag,
							MVector &force, 
							MVector &torque
							) ;
	void calcBounceVectors(
						   const MVector &localVelocity,
						   const MVector &localAccel,
						   const double &bounceSpinValue ,
						   const double &bounceDeflectValue ,
						   MVector &bounceForce,
						   MVector &bounceTorque
						   );
	
private:
		MPointArray m_vertices;
	MVectorArray m_normals;
	MVectorArray m_centers;
	MDoubleArray m_areas;
	unsigned m_size;
	
	
};

#endif


