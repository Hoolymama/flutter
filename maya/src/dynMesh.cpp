/*
 *  dynMesh.cpp
 *  jtools
 *
 *  Created by Julian Mann on 02/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#include <maya/MObject.h>
#include <maya/MFnMesh.h>

#include <maya/MItMeshPolygon.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MVector.h>
#include "errorMacros.h"
#include "dynMesh.h"

inline MVector  crossProdSum(
							 const MVector &p1,
							 const MVector &p2,
							 const MVector &p3
							 )
{
	
	return (p1^p2)+(p2^p3)+(p3^p1);
}

inline MVector triCenter( 	
							const MPoint &p1,
							const MPoint &p2,
							const MPoint &p3
							)
{
	return ((p1 + p2 +p3 ) * 0.33333333333333);
}


dynMesh::dynMesh(){}

dynMesh::~dynMesh(){}

dynMesh::dynMesh(const MObject &meshObject, bool normalize){
	
	MStatus st;
	MString method("dynMesh::dynMesh");
	
	MFnMesh meshFn(meshObject);
	meshFn.getPoints(m_vertices) ;
	
	MItMeshPolygon polyIter(meshObject, &st);er;
	int triCount;
	MPointArray triPoints; // needed for getTriangle
	MIntArray triVerts; // needed for getTriangle
	double totalArea = 0.0;
	m_size = 0;
	for( ; !polyIter.isDone(); polyIter.next()) {
		if (polyIter.hasValidTriangulation() ) {
			st = polyIter.numTriangles(triCount);
			for (int tri = 0;tri < triCount;tri++) {
				// use the local space of the triangle
				st = polyIter.getTriangle( tri, triPoints, triVerts, MSpace::kObject );
				
				MPoint & A = triPoints[0];
				MPoint & B = triPoints[1];
				MPoint & C = triPoints[2];
				// cerr << A << " " << B << " " << C << endl;
				
				
				// get the normals - note the sum of 
				// the lengths of these normals is 
				// is a factor of the total area
				
				//MVector normal = crossProdSum(A-B,B-C,C-A);
				MVector normal = crossProdSum(A,B,C);
				double area = normal.length();
				m_normals.append(normal);
				m_areas.append(area);
				m_centers.append(triCenter(A,B,C));
				totalArea += area;
				m_size++;
			}
		}
	}

	//totalArea = totalArea * 0.5;
	if (normalize) {
		// divide everything by totalArea.
		double recip = 1.0 / totalArea;
		for (unsigned i = 0; i<m_size;i++) {
			m_normals[i] *= recip;
			m_centers[i] *= recip;
			m_areas[i]	 *= recip;
		}
	}
}
/*
 void dynMesh::calcForceAndTorque(const MVector &localVelocity, MVector &force, MVector &torque) {
	 // calculate the force and torque vectors from triangle's average velocity 
	 
	 torque = MVector::zero;
	 force = MVector::zero;
	 
	 for (unsigned i=0;i<m_size;i++){
		 
		 
		 
		 double dot = (localVelocity * m_normals[i]);
		 MVector thisForce =  -dot * m_normals[i].normal();
		 torque += thisForce^m_centers[i];
		 force += thisForce;
	 }
	 // cerr << force << endl;
 }
 */

void dynMesh::calcForceAndTorque(
									double dt,
								   const MVector &localVelocity,
								   const MVector &localOmega, 
								   const double &forceMag,
								   const double & torqueMag,
								   MVector &force, 
								   MVector &torque) {
	// calculate the force and torque vectors from each triangle's  velocity 

	torque = MVector::zero;
	force = MVector::zero;

	for (unsigned i=0;i<m_size;i++){
		// the triangle center may be offset from its center of mass (local zero) - 
		// this is why we add the cross prod of spin and center offset to the velocity
		MVector triVelocity = localVelocity + (m_centers[i]^localOmega);
		double dot = (triVelocity * m_normals[i]);
		//dot = fabs(dot); // this makes it double sided (only consider drag against direction of motion)
		MVector thisForce =  -dot * m_normals[i].normal();
		torque += thisForce^m_centers[i];
		force += thisForce;
	}
	force *= forceMag;
	torque *= torqueMag;

}

void dynMesh::calcForceAndTorque(
								   const MVector &localVelocity,
								   const MVector &localOmega, 
								   const double &forceMag,
								   const double & torqueMag,
								   MVector &force, 
								   MVector &torque) {
	// calculate the force and torque vectors from each triangle's  velocity 
	
	torque = MVector::zero;
	force = MVector::zero;
	
	for (unsigned i=0;i<m_size;i++){
		
		MVector triVelocity = localVelocity + (m_centers[i]^localOmega);
		double dot = (triVelocity * m_normals[i]);
		MVector thisForce =  -dot * m_normals[i].normal();
		torque += thisForce^m_centers[i];
		force += thisForce;
	}
	force *= forceMag;
	torque *= torqueMag;
	
}

void dynMesh::calcBounceVectors(
								  const MVector &localVelocity,
								  const MVector &localAccel,
								  const double &bounceSpinValue ,
								  const double &bounceDeflectValue ,
								  MVector &bounceForce,
								  MVector &bounceTorque
								  ) {
	// we assume the cross prod of Accel^Velocity is the spin axis
	bounceTorque = (localAccel^localVelocity) ;
	// also we assume Accel vector is the surface normal
	if (bounceDeflectValue != 0.0) {
		int minIndex = -1;
		double minValue = 100000000;
		MVector N = localAccel.normal();
		for (unsigned i = 0;i< m_vertices.length();i++) {
			// project verts onto accel
			double dot = MVector(m_vertices[i])*N;	
			if (dot < minValue) {
				minValue = dot;
				minIndex = i;
			}
		}
		if (minIndex > -1) {
			// we know the lowest vertex
			MVector cross = N^MVector(m_vertices[minIndex]);
			MVector dir = N^cross;
			double dotV = localVelocity*N;
			bounceForce = (dir*bounceDeflectValue*dotV );
		}
	}
	bounceTorque *=bounceSpinValue;
}
