#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"
#include "flutter.h"

MStatus initializePlugin( MObject obj)
{

	MStatus st;
	
	MString method("initializePlugin");

	MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);
	st = plugin.registerNode( "flutter", flutter::id, flutter::creator, flutter::initialize); ert;	

	// register your plugin's nodes, commands, or whetever here.
		MGlobal::executePythonCommand("import flutter;flutter.load()");

	return st;

}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;

	MString method("uninitializePlugin");

	MFnPlugin plugin( obj );
	st = plugin.deregisterNode( flutter::id );er;

	// deregister your plugin's nodes, commands, or whetever here.


	return st;
}


