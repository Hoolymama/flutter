/*
*  flutter.h
	*  jtools
	*
	*  Created by Julian Mann on 02/12/2006.
	*  Copyright 2006 hooly|mama. All rights reserved.
	*
*/
// flutter - fast simulation of leaf flight
// Forces and torques are calculated for the particle based on a mesh.
// The resulting force vector and rotation are sent back to the particle object.
//
#ifndef _flutter
#define _flutter

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 
#include <maya/MTime.h> 
#include <maya/MGlobal.h>
#include <maya/MVectorArray.h>

	class flutter: public  MPxNode
{

public:
	flutter() ;

	virtual ~flutter();

	static void		*creator();
	static MStatus	initialize();

// will compute output force.
	virtual MStatus	compute( const MPlug& plug, MDataBlock& data );

	static MTypeId	id;

// inputs
	static MObject	aCurrentTime;
	static MObject	aPosition;
	static MObject	aVelocity;
	static MObject	aMass;

	static MObject	aAcceleration;
	static MObject	aEvent;
	static MObject	aPhi;
	static MObject	aOmega;
	static MObject	aMeshIndex;
	static MObject	aForceMagPP; 
	static MObject	aTorqueMagPP; 
	static MObject	aTorqueConservePP;	
	//static MObject	aExtraInputData;

	static MObject	aInputMeshes;

	static MObject	aNormalize;
	static MObject	aForceMag; 
	static MObject	aTorqueMag; 
	static MObject	aTorqueConserve; 



	static MObject	aDoBounce; 
	static MObject	aBounceSpin; 
	static MObject	aBounceSpinThreshold; 
	static MObject	aBounceDeflect; 
	static  MObject	aIRotateOrder;
	static  MObject	aOutputUnit;

// outputs

	static MObject	aOutForce; 	
	static MObject	aOutPhi; 	
	static MObject	aOutOmega; 	
	static MObject	aOutRotation;
	//static MObject	aExtraOutData; 	



private:

	MStatus calculateForce
		(
		double dT,
		MDataBlock &data, 
		const MVectorArray &points,
		const MVectorArray &velocities,
		const MVectorArray &accelerations,
		const MDoubleArray &events,
		const MVectorArray &phi,
		const MVectorArray &omega,
		const MDoubleArray &meshIndex,
		const MDoubleArray &forceMagPP,
		const MDoubleArray &torqueMagPP,
		const MDoubleArray &torqueConPP,
		MVectorArray &outForce ,
		MVectorArray &outPhi,
		MVectorArray &outOmega,
		MVectorArray &outRotation
		);


	MTime lastTimeIEvaluated;


};
#endif
